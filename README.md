    *theme       : E-comunity .
    *subject     : Online Book Trading .

Project description:
    - C2C platform that allows users to exchange books .
    - A member can :
                        * add his book to his library.
                        * exposes his library 
                        * search available books (by name , author , ISBN ...)
                        * request trade with an other member
                        * accept or deny trade request 

    - A lamda user (no account required) can :
                        * search available books (by name , author , ISBN ...)
